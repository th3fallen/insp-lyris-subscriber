<?php

/**
 * mailer
 *
 * @package Lyris Auto Subscriber
 * @author Clark Tomlinson
 * @copyright Copyright (c) 2012
 * @version 1.0
 * @access public
 */
class mailer {

//set data varables
    public $list1 = '';
    public $list2 = '';
    public $lists = array();
    public $siteid = '';
    public $redirect = '';
    public $email = '';
    public $welcome = '';
    public $data = array();
    public $ajax = false;

    /**
     * mailer::process()
     *
     * @param array $data
     * @return Status of submission to lyris
     */
    public function process($data) {
        $this->enumerate_lists($data);
        $this->multi_submit();
    }

    /**
     * mailer::enumerate_lists()
     *
     * @param array $data
     * @return number of lists
     */
    private function enumerate_lists($data) {
        foreach ($data as $key => $value) {
            $list = strpos($key, 'list');
            if ($list !== false) {
                $lists[$key] = $value;
                unset($data[$key]);
            }
        }
        $this->lists = $lists;
        $this->check_data($data);
    }

    /**
     * mailer::check_data()
     *
     * @param array $data
     * @return Outputs errors if required data is missing
     */
    private function check_data($data) {
        if (!array_key_exists('list1', $this->lists)) {
            $error[] = 'Primary List(list1) is Missing';
        }

        if (!array_key_exists('siteid', $data)) {
            $error[] = 'Site Id(siteid) is Missing';
        }

        if (!array_key_exists('redirection', $data)) {
            $error[] = 'Redirection(redirection) is Missing';
        }

        if (!array_key_exists('email', $data)) {
            $error[] = 'Email is Missing';
        }

        if (!array_key_exists('welcome', $data)) {
            $data['welcome'] = '';
        }

        if (array_key_exists('ajax', $data)) {
            $this->ajax = true;
        }

        if (!empty($error)) {
            $this->display_errors($error);
        } else {
            $this->siteid = $data['siteid'];
            $this->email = $data['email'];
            $this->redirect = $data['redirection'];
            $this->welcome = $data['welcome'];
        }
    }

    /**
     * mailer::multi_submit()
     *
     * @return  Submits Multiple Lists
     */
    private function multi_submit() {
        foreach ($this->lists as $key => $value) {
            $listdata = array(
                'siteid' => $this->siteid,
                'mlid' => $value,
                'email' => $this->email,
                'redirection' => $this->redirect,
                'uredirection' => 'http://',
                'activity' => 'submit',
                'append' => '',
                'update' => '',
                'double_optin' => '',
                'welcome' => $this->welcome
            );
            $this->submit($listdata);
        }
    }

    /**
     * mailer::submit()
     *
     * @return Submits Singular List to lyris
     */
    private function submit($data) {
// set POST variables
        $url = 'http://www.elabs10.com/functions/mailing_list.html';
// get Fields
        $data = http_build_query($data);
// open connection
        $ch = curl_init();
// set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
// execute post
        $result = curl_exec($ch);
// close connection
        curl_close($ch);
        //echo $result;
    }

    /**
     * mailer::display_errors()
     *
     * @param array $error
     * @return Returns div's of errors if ajax flag is false else returns json string
     */
    private function display_errors($error) {
        if ($this->ajax == false) {
            foreach ($error as $error) {
                echo '<div class="error">' . $error . '</div>';
            }
            die();
        } else {
            echo json_encode($error);
        }
    }

    /**
     * mailer::output_data()
     *
     * @return Debugging Output of data
     */
    public function output_data() {
        echo '<pre>';
        print_r($this->data);
        echo '</pre>';
    }

}

?>