<?php

/**
 *
 * @author Clark Tomlinson
 * @version 1.0
 * @copyright 2012
 */

//force acception of cross domain ajax posts
header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
header('Access-Control-Allow-Methods: POST, GET, OPTIONS');
header('Access-Control-Max-Age: 1000');
header('Access-Control-Allow-Headers: Content-Type');

if (!isset($_POST['siteid'])) {
    die('Direct Access Not Permitted or SiteID is missing');
}

//load mailer class
require_once('mailer_class.php');
//instanciate the mailer class
$mail = new mailer();

//send input to mailer class for processing and submission
$mail->process($_POST);


//debugging output of data received by class
//$mail->output_data();
?>